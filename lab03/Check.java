//John Taulane
//Cse 002
//9/14/2018

//This program takes input from the user about the cost a check, the amount of people and the percentage tip
//and then outputs how much each person should pay to split the check

import java.util.Scanner;
//Imports the scanner class
public class Check{  
  public static void main(String[] args){
    //main method starts here ^^^^
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Enter the total cost of the check in the form xx.xx");
    double checkCost = myScanner.nextDouble();
    //This allows the user to input the cost of the check and store it as double checkCost
    System.out.println("Enter the percentage of the check that you wish to tip in the form xx");
    double tipPercent = myScanner.nextDouble();
    tipPercent /=100;
    //the "/=" part converts the input into a decimal value
    System.out.println("How many people are splitting the check?");
    int numPeople = myScanner.nextInt();
    //We use nextInt(); here rather than nextDouble because the amount of people is stored as an integer
    
    double totalCost = checkCost * (1.0 + tipPercent);
    double costPerPerson = totalCost / numPeople;
    
    int dollars = (int)costPerPerson;           //Gets the amount in whole dollars
    int dimes = (int)(costPerPerson * 10) % 10; //Gets the amount in the 10's decimal place. The modlus operator gets the remainder
    int pennies = (int)(costPerPerson * 100) % 10;//Gets the digit in the 100's decimal place.
    
    System.out.println("Each person owes $" + dollars + "." + dimes + pennies);
    //Prints out the result of how much everyone needs to pay in the form xx.xx
  }//End of the main method
}//End of the class