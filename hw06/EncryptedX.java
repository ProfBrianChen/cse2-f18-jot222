//John Taulane
//CSE002
//HW6
//10/19/18

//This program uses nested loops to print an x encrypted in a bunch of asterisks

import java.util.Scanner;
public class EncryptedX{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    int gridSize=101; 

    
    while(gridSize < 0 || gridSize >100){//Automatically goes into the while loop. Makes it so that it repeats if the user doesn't enter a valid integer
      System.out.println("Enter an integer that is between 0-100");
      gridSize = myScanner.nextInt();//Sets the size of the square grid to the number inputted
    }
   
    for(int i=0; i<=gridSize; ++i){//Makes the right amount of rows
      
      for(int k=0; k<=gridSize; ++k){//Makes the same amount of colums
        
        if(i==k || i==gridSize-k){//Creates the space in the grid so that it forms an x
          System.out.print(" ");
        }
        else{
          System.out.print("*");//Fills the rest of the grid in with asterisks
        }
      }
      
      System.out.println("");//Makes a new line after each row is printed
    }
    
    
  }
}