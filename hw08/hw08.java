//John Taulane
//CSE002
//11-13-2018
//This program gets a deck of cards, shuffles it, and then prints out a hand of 5 cards repeatedly if the user wishes

import java.util.Scanner;
import java.util.Random;
public class hw08{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    
//suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    System.out.println("How many cards do you wish to be dealt? ");
    int numCards = scan.nextInt(); 
    int again = 1; 
    int index = 51;
    
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
    } 
   
    System.out.println();
    printArray(cards);//Prints the unshuffled cards
    System.out.println();
    shuffle(cards); //Shuffles the cards
    printArray(cards);//Prints the shuffled cards
   
    while(again == 1){ 
      if(index-numCards<0){//Enters when there aren't enough cards for the hand
        System.out.println("RESHUFFLING");
        shuffle(cards);//Shuffles the cards again to make a new order for the deck
        printArray(cards);
        index=51;//Resets the index
      }
      hand = getHand(cards,index,numCards); //Gets a hand of 5 cards
      printArray(hand);//Prints those 5 cards
      index = index - numCards;//Makes it so the next 5 cards are dealt for the next hand
      System.out.println();
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }
  
  } //End of the main method
  
  public static void printArray(String[] cards){
    for(int i =0; i<cards.length;i++){
      System.out.print(cards[i]+" ");
    }
  }
  
  public static void shuffle(String[] cards){
    for(int i=0;i<100;i++){//Randomly switches cards 100 times
      Random randomGenerator = new Random();
      int randNum = randomGenerator.nextInt(52);//rand int from 0-51
      String tempValue = cards[0];//temporary value used to switch cards to a different index
      cards[0]=cards[randNum];
      cards[randNum]=tempValue;
    }
    System.out.println("SHUFFLED");
  }
  
  public static String[] getHand(String[] cards,int index,int numCards){
    int x=0;
    String[]hand = new String[numCards];//Hand = 5
    for(int i=index;i>index-numCards;i--){
      hand[x]=cards[i];//hand[0]=cards[51], hand[1]=cards[50] ...
      ++x;
    }
    System.out.println();
    System.out.println("HAND");
    return hand;
  }
  
}
