//John Taulane
//CSE002
//11-16-2018
//LAB 09

import java.util.*;
public class lab09{
  public static void main(String [] args){
    int[] array0 = {1,2,3,4,5,6,7,8};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    print(inverter(array0));
    print(inverter2(array1));
    int []array3=inverter(array2);
    print(array3);
  }//End of main method

  public static int[] copy(int[] myArray){
    int[] copyArray = new int[myArray.length];
    for(int i =0; i<myArray.length; i++){
      copyArray[i]=myArray[i];
    }
    return copyArray;
  }//End of copy method
  
  public static int[] inverter(int[] myArray){
    int tempVal=0;
    for(int i =0; i<myArray.length/2;i++){
      tempVal = myArray[i];
      myArray[i]=myArray[myArray.length-1-i];
      myArray[myArray.length-1-i]=tempVal;
    }
    return myArray;
  }//End of inverse method
  
  public static int[] inverter2(int[] myArray){
    int [] copyArray = copy(myArray);
    int tempVal=0;
    for(int i =0; i<copyArray.length/2;i++){
      tempVal=myArray[i];
      copyArray[i]=copyArray[myArray.length-1-i];
      copyArray[myArray.length-1-i]=tempVal;
    }
    return copyArray;
  }//End of inverse2 method
  
  
  public static void print(int[] myArray){
    for(int i=0;i<myArray.length;i++){
      System.out.print(myArray[i]+" ");
    }
    System.out.println("");
  }


}