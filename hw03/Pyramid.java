//John Taulane
//CSE002
//9/14/2018

//This program outputs the area of a pyramid based off of user-inputted dimensions

import java.util.Scanner;
//Imports the scanner class
public class Pyramid{
  public static void main(String[] args){
    //Start of the main method
    Scanner input = new Scanner(System.in);
     //Creates a new scanner called "input"
   
    System.out.println("Enter the height of your pyramid");
    double height = input.nextDouble();
    System.out.println("Enter the length of your pyramid");
    double length = input.nextDouble();
    System.out.println("Enter the width of your pyramid");
    double width = input.nextDouble();
    //Gets the user dimensions of the pyramid and stores them as doubles
   
    double volume = (length * width * height)/3;
    //Calculates the volume and stores it as double volume
    System.out.println("The volume of your pyramid is " + volume);
    //Prints the volume of the pyramid
  }
}