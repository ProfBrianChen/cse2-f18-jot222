//John Taulane
//CSE002
//9/14/2018

//This program converts the amount of user inputted rain into cubic miles

import java.util.Scanner;
//Imports the scanner class
public class Convert{
  //starts the class
  public static void main(String[] args){
    //Starts the main method
    Scanner input = new Scanner(System.in);
    //Creates a new scanner object called input
    
    double acres;
    double inchesRain;
    double gallons;
    int gallonsPerAcre = 27154;
    //The amount of gallons in an acre
    double gallonsPerCubicMile = 1101117130711.3;
    //The amount of gallons in a cubic mile
    
    System.out.println("Enter the area of the land affected in acres");
    acres = input.nextDouble();
    System.out.println("Enter the rainfall in the affected area in inches");
    inchesRain = input.nextDouble();
    //The 4 lines above collect the inputted data and store them as double variables
    gallons = (gallonsPerAcre * acres * inchesRain);
    //Converts the inputted data to gallons
    double cubicMiles = (gallons / gallonsPerCubicMile);
    //converts the gallons into cubic miles and stores it as a double
    System.out.println(cubicMiles + " cubic miles of rain");
    //Outputs the amount of cubic miles of rainfall
  }
}