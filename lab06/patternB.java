//John Taulane
//CSE002
//10/12/18

//This program prints various patterns using nested loops
import java.util.Scanner;
public class patternB{
  public static void main(String[] args){
    Scanner myScan = new Scanner(System.in);
    int check = 0;
    int userInt=0;
    
    while(check==0){//Automatically enters the while loop
      System.out.println("Enter an integer between 1 and 10");
      userInt=myScan.nextInt();
      
      if(userInt>=1 && userInt<=10){//Exits the while loop when the user's number is between 1 and 10
        check=1;
      }
      else{//Enters else statement when the user doesn't enter an integer in the range
        System.out.println("Invalid entry");
      }
    }
    
    for(int rows=userInt;rows>0;rows--){
      for(int i=1;i<rows;i++){
        System.out.print(i+" ");
      }
      System.out.println(rows);
    }
  
  }
}