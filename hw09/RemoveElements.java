//John Taulane
//11/19/2018
//CSE002
//Prof Carr
//HW 09 Pt 2
import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
   
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  public static int[] randomInput(){
    int [] num = new int [10];
    Random randGen = new Random();
    for(int i=0;i<num.length;i++){
      int randNum = randGen.nextInt(10);
      num[i]=randNum;
    }
    return num;
  }
  
  public static int[] delete(int[]list, int pos){
    Scanner myScan = new Scanner(System.in);
    int[] deleteArray = new int[list.length-1];
    while(pos>list.length-1 || pos<0){//Check deleted index exists
      System.out.println("Position is out of bounds. Enter a new one.");
      pos = myScan.nextInt();
    } 
    for(int i=0; i<deleteArray.length;i++){//Goes through list[] and copies all of the values that aren't the deleted index into deleteArray[]
      if(i<pos){
        deleteArray[i]=list[i];
      }
      else if(i>=pos){
        deleteArray[i]=list[i+1];
      }
    }//End of the for loop
    return deleteArray;
  }//End of the delete method
  
  public static int[] remove(int[] list, int target){
    int counter=0;
    for(int i=0;i<list.length;i++){//This for loop counts how many ocurrences of the target so that we know the length of the new array
      if(list[i]==target){counter++;}
    }//End of the for loop
    
    int[] removeArray = new int[list.length-1-counter];
    
    
    int index=0;//Increases everytime a value is removed
    for(int k=0;k<removeArray.length;k++){
      if(target==list[k+index]){
        ++index;
        removeArray[k]=list[k+index];
        }
      else{
        removeArray[k]=list[k+index];
      }
    }
    return removeArray;
  }//End of the remove method


}

