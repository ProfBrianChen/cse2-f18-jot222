//John Taulane
//11-18-2018
//CSE002
//Professor Carr
//HW 09 Part 1

import java.util.*;
public class CSE2Linear{
  public static void main(String[] args){
    System.out.println("Enter 15 final grades in ascending order: ");
    int [] finalGrades = new int[15];
    Scanner myScan = new Scanner(System.in);
    
    for(int i=0;i<finalGrades.length;i++){
     
      boolean checkInt = true;
      boolean checkRange = true;
      boolean checkGreaterThan = true;
      
      while(checkInt){
        while(checkRange){
          while(checkGreaterThan){
            System.out.println(i+" Enter a grade");
            
            if(myScan.hasNextInt()){
              finalGrades[i] = myScan.nextInt();
              checkInt=false;
              if(finalGrades[i]>=0 && finalGrades[i]<=100){
                checkRange=false;
                if(i==0){
                  checkGreaterThan=false;
                }
                else if(finalGrades[i]>=finalGrades[i-1]){
                  checkGreaterThan=false;
                }
                else{System.out.println("ERROR! Enter a number greater than or equal to the previous grade!");}
              }
              else{System.out.println("ERROR! ENTER A NUMBER WITHIN THE RANGE 0-100!");}
            }
            
            else{
              System.out.println("ERROR! ENTER AN INTEGER!");
              myScan.next();
              checkInt = true;
            }
          }//End check Greaterthan loop
        }//End checkRange loop
      }//End checkInt loop
    }//End of the for loop
    
    System.out.println(Arrays.toString(finalGrades));
    System.out.println("Enter a grade to search for: ");
    int key = myScan.nextInt();
    binarySearch(key, finalGrades);
    
    scramble(finalGrades);
    
    System.out.println("Enter a grade to search for: ");
    key = myScan.nextInt();
    linearSearch(key, finalGrades);
    
    
  }//End of the main method
  
 
 ///////////////////////////////////////////////////////////////////////////////////////////////////////// 
  
  public static void binarySearch(int key, int[] finalGrades){
    int lowestIndex=0;
    int highestIndex=finalGrades.length-1;
    int iterationCounter=0;
    boolean inArray=false;
    
    while(highestIndex >= lowestIndex){//Repeats until either found or not in the array
      int midIndex = (highestIndex+lowestIndex)/2;
      ++iterationCounter;//Increases the amount of iterations each time the loop executes
      
      if(key > finalGrades[midIndex]){
        lowestIndex = midIndex + 1;
      }
      else if(key < finalGrades[midIndex]){
        highestIndex = midIndex - 1;
      }
      else if(key == finalGrades[midIndex]){
        inArray=true;
        break;//Ends if key is found
      }
    }
    if(inArray){
      System.out.println("The key "+key+" was found after "+iterationCounter+" iterations.");
    }
    else{
      System.out.println("The key "+key+" was NOT found after "+iterationCounter+" iterations.");
    } 
  }//End of binary search method
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  public static void scramble(int[] finalGrades){
    Random randGen = new Random();
    int tempVal=0;
    for(int i=0;i<50;i++){//Randomly shuffles 50 times
      int randNum = randGen.nextInt(finalGrades.length);
      tempVal=finalGrades[0];
      finalGrades[0]=finalGrades[randNum];
      finalGrades[randNum]=tempVal;
    }
    System.out.println("Scrambled: ");
    System.out.println(Arrays.toString(finalGrades));
  }//End of the scramble method
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  
  public static void linearSearch(int key, int[] finalGrades){
    int iterations = 1;
    for(int i=0; i<finalGrades.length;i++){
      if(finalGrades[i]==key){
        System.out.println("The key "+key+" was found after "+iterations+" iterations");
      }
      else{
        iterations++;
      }
    }
    if(iterations>finalGrades.length){
      System.out.println("The key "+key+" was not found after "+iterations+" iterations");
    }
  }//End of the linear search method

}