//John Taulane
//10/26/18
//This program will randomly create a story. This program will focus on using methods.

import java.util.Random;//Imports the random class
import java.util.Scanner;//Imports the scanner class
public class storyGenerator{
  
  public static void main(String[] args){
    
    finalParagraph();//Calls the finalParagraph method which puts everything together
  }//End of the main method
  
  public static String thesisStatement(){//This method prompts the user with random thesis sentences until they pick one they like
    Scanner myScanner = new Scanner(System.in);
    int check=1;
    String subj="";//This is the subject to be returned to the main method
    
    while (check==1){//Repeats until the user enters a number that is not 1
    String adjective = genAdjective();//Calls the various methods that randomly generate words
    String adjective2 = genAdjective();
    String subject = genSubject();
    String verb = genVerb();
    String object = genObject();
    
     System.out.println("The " + adjective +" "+ subject +" "+verb+" the " +adjective2 +" "+object);
     
     System.out.println("Do you want to generate a different thesis sentence? Enter 1 for yes, any other integer for no");
     check = myScanner.nextInt();//if it's 1 the while loop will run again
     subj = subject;
    }
    return subj;//Returns the subject of the sentence so it can be used in the following sentences
  }
  
  public static void actionSentence(String actionSubject){//Creates an action sentence using the same subject as the thesis
    String adjective = genAdjective();
    String object = genObject();
    String verb = genVerb();
    
    Random randomGenerator = new Random();
    int randNum= randomGenerator.nextInt(2);//This random integer will make it so that "it" is used randomly
    
    if(randNum==0){       //50% chance of using it as the subject
      actionSubject = "It";
    }
    else{               //50% chance of using the previous subject as the subject
      actionSubject = "That " + actionSubject;
    }
    
    System.out.println(actionSubject + " looked at a " + object +" and it " + verb + " that " + object);
  }
  
  public static void conclusionSentence(String conclusionSubject){//Creates the final sentence with the same subject
    String verb = genVerb();
    String object = genObject();
    System.out.println("In the end, that "+ conclusionSubject + " actually "+ verb + " a " + object +" and liked it too!");
  }
  
  public static void finalParagraph(){//Puts everything together. This is the method that is called by main
    String subject = thesisStatement();//This calls the thesis Statement method which prints the thesis and stores subject as the subject of the sentence
    Random randomGenerator = new Random();
    int randSentences = randomGenerator.nextInt(5);//Creates a random integer from 0-4
    
    for(int i=0; i<=randSentences; i++){//The for loop makes it so that it prints a random amount of action sentences from 1-4
    actionSentence(subject);
    }
    conclusionSentence(subject);
  }
  
  public static String genAdjective(){//Method that acts as a random adjective generator
    String adjective="";
    Random randomGenerator = new Random();
    int randNum= randomGenerator.nextInt(10);
    
    switch(randNum){
      case 0:
        adjective = "Purple";
        break;
      case 1:
        adjective = "Ugly";
        break;
      case 2:
        adjective = "Greedy";
        break;
      case 3:
        adjective = "Socialist";
        break;
      case 4:
        adjective = "Forgetful";
        break;
      case 5:
        adjective = "Foreign";
        break;
      case 6:
        adjective = "Unique";
        break;
      case 7:
        adjective = "Absurd";
        break;
      case 8:
        adjective = "Infallible";
        break;
      case 9:
        adjective = "Corrupt";
        break;
      default:
        adjective = "MISTAKEN";
    }
    return adjective;
  }
  
  public static String genSubject(){//Start of the random subject generator method
    String subject="";
    Random randomGenerator = new Random();
    int randNum= randomGenerator.nextInt(10);
    
    switch(randNum){
      case 0:
        subject = "Farmer";
        break;
      case 1:
        subject = "Wolverine";
        break;
      case 2:
        subject = "Pacifist";
        break;
      case 3:
        subject = "Scientist";
        break;
      case 4:
        subject = "Dog";
        break;
      case 5:
        subject = "Boss";
        break;
      case 6:
        subject = "Statue of liberty";
        break;
      case 7:
        subject = "mountain";
        break;
      case 8:
        subject = "clown";
        break;
      case 9:
        subject = "bear";
        break;
      default:
        subject = "MISTAKE";
    }
    return subject;
  }
  
  public static String genVerb(){//Random verb generating method
    String verb="";
    Random randomGenerator = new Random();
    int randNum= randomGenerator.nextInt(10);
    
    switch(randNum){
      case 0:
        verb = "Liberated";
        break;
      case 1:
        verb = "Acknowledged";
        break;
      case 2:
        verb = "Ate";
        break;
      case 3:
        verb = "Dunked on";
        break;
      case 4:
        verb = "Forgave";
        break;
      case 5:
        verb = "Conquered";
        break;
      case 6:
        verb = "Harrassed";
        break;
      case 7:
        verb = "Drank";
        break;
      case 8:
        verb = "Beat up";
        break;
      case 9:
        verb = "Yelled at";
        break;
      default:
        verb = "MISTAKE";
    }
    return verb;
  }
  
  public static String genObject(){//Random object generator method
    String object="";
    Random randomGenerator = new Random();
    int randNum= randomGenerator.nextInt(10);
    
    switch(randNum){
      case 0:
        object = "Townspeople";
        break;
      case 1:
        object = "Dragon";
        break;
      case 2:
        object = "Computer";
        break;
      case 3:
        object = "Train";
        break;
      case 4:
        object = "Commuter";
        break;
      case 5:
        object = "IPOD Nano";
        break;
      case 6:
        object = "Pencil";
        break;
      case 7:
        object = "Desk";
        break;
      case 8:
        object = "Basketball";
        break;
      case 9:
        object = "Cat";
        break;
      default:
        object = "MISTAKE";
    }
    return object;
  }
  

}