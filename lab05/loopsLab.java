//John Taulane
//CSE002
//  10/05/18

//This program lets the user input various variable type and the uses infinte loops to make sure the right type is inputted

import java.util.Scanner;//Imports the java class

public class loopsLab{
 public static void main(String[] args){
  Scanner myScanner=new Scanner(System.in);//Creates the Scanner myScanner
  //Initializes and declares the various variables that the user is going to imput the values of
   int courseNum=0;
   String departmentName = " ";
   int daysPerWeek=0;
   int time = 0;
   int numStudents = 0;
   String name= " ";
//Initializes and declares the various booleans used to stop the while loops
   boolean check1 = true;
   boolean check2 = true;
   boolean check3 = true;
   boolean check4 = true;
   boolean check5 = true;
   boolean check6 = true;
 
   while(check1){//Stars as true
     System.out.println("Enter your course number");     
     boolean checkCourse=myScanner.hasNextInt();//Is true when the user inputs an int
    
     if(checkCourse){                      //Enter if user inputs an integer
       courseNum = myScanner.nextInt();
       check1=false;                       //ends the while loop
     }
     else{
       myScanner.next();                   //clears the "conveyor belt"
       System.out.println("Invalid. Enter course number as an integer");//Continues back to the top of the loop
     }
   }   //end of the while staement

   while(check2){//True
     System.out.println("Enter the department name");
     boolean checkDepartment=myScanner.hasNext();      //check department is true when the user inputs a string
    
     if(checkDepartment){                        //Enter when string is input by the user
       departmentName = myScanner.nextLine();
       check2=false;                             //Ends the while loop
     }
     else{
       myScanner.next();
       System.out.println("Invalid enter department name as a string");//Continues back the top of the loop
     }
   }

   while(check3){                        //Lines 56-109 do the same things that the previous two while statements did
     System.out.println("Enter the amount of days per week that the class meets");
     boolean checkDays = myScanner.hasNextInt();

     if(checkDays){
       daysPerWeek = myScanner.nextInt();
       check3=false;
     }
     else{
       myScanner.next();
       System.out.println("Invalid. Enter amount of days per week as an integer");
     }
   }

   while(check4){
     System.out.println("Enter the time the class meets in the form 0000 to 2400 hrs");
     boolean checkTime = myScanner.hasNextInt();

     if(checkTime){
       time=myScanner.nextInt();
       check4=false;
     }
     else{
       myScanner.next();
       System.out.println("Invalid. Enter the time as an integer");
     }
   }

   while(check5){
     System.out.println("Enter the amount of students in the class");
     boolean checkStudents = myScanner.hasNextInt();
    
     if(checkStudents){
       numStudents=myScanner.nextInt();
       check5=false;
     }
     else{
       myScanner.next();
       System.out.println("Invalid. Enter the number of students as an integer");
     }
   }

   while(check6){
     System.out.println("Enter the professor's name");
     boolean checkName = myScanner.hasNext();
     if(checkName){
       name=myScanner.nextLine();
       check6=false;
     }
     else{
       myScanner.next();
       System.out.println("Invalid name");
     }
   }

//Outputs what the user put in in a meaningful output statement
   System.out.println("Course number: "+courseNum);
   System.out.println("Department: " + departmentName);
   System.out.println("Number of days class takes place: "+daysPerWeek);
   System.out.println("Time of class " + time);
   System.out.println("Meetings per week: "+ daysPerWeek);
   System.out.println("Professor's name: "+ name);

 }//End of the main method
}//End of the class

