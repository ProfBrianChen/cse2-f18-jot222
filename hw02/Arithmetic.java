//John Taulane
//CSE 002
//Date: 9:10/2018

//This program calculates and displays the total cost of a shopping trip where pants, sweatshirts, and belts were purchased.

public class Arithmetic{
  public static void main (String[] args){
    int numPants = 3;
    double pantsPrice = 34.98;
    //Price and number of pants
    int numShirts = 2;
    double shirtPrice = 24.99;
    //price and number of sweatshirts
    int numBelts = 1;
    double beltPrice = 33.99;
    //price and number of belts
    double paSalesTax = .06;
    //The sales tax in the state of Pennsylvania is 6%
    double totalCostOfPants = (numPants * pantsPrice);
    double totalCostofShirts = (numShirts * shirtPrice);
    double totalCostofBelts = (numBelts * beltPrice);
    //gets the total cost of each type of clothing before tax and stores each as a double variable
    double pantsTax = (totalCostOfPants * paSalesTax);
    double shirtsTax = (totalCostofShirts * paSalesTax);
    double beltsTax = (totalCostofBelts * paSalesTax);
    //gets the amount of sales tax for each indivual type of clothing and stores it as a a double variable
    
    pantsTax *= 100;
    shirtsTax *= 100;
    beltsTax *= 100;
    
    pantsTax = (int)pantsTax;
    shirtsTax = (int)shirtsTax;
    beltsTax = (int)beltsTax;
    
    pantsTax /= 100;
    shirtsTax /= 100;
    beltsTax /= 100;
    //Lines 23-33 make it so that there is only two decimal places in the tax
    System.out.println("The total cost of all of the pants is $" + totalCostOfPants + " with $" + pantsTax + " in tax");
    System.out.println("The total cost of all of the sweatshirts is $" + totalCostofShirts + " with $" + shirtsTax + " in tax");
    System.out.println("The total cost of all of the belts is $" + totalCostofBelts + " with $" + beltsTax + " in tax");
    //Prints the cost and tax for each indivual type of clothing purchased
    double totalCostNoTax = (totalCostOfPants + totalCostofShirts + totalCostofBelts);
    double totalSalesTax = (pantsTax + shirtsTax + beltsTax);
    double totalCostWithTax = (totalCostNoTax + totalSalesTax);
    //declares and initializes the total cost of everything with and without tax as well as the total tax as double variables
    System.out.println("The total without sales tax is $" + totalCostNoTax);
    System.out.println("The total sales tax is $" + totalSalesTax);
    System.out.println("The total purchase with sales tax included is $" + totalCostWithTax);
    //Prints the final cost, before and after sales tax as well as the total sales tax
  }
  
}