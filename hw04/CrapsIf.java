//John Taulane
//CSE002
//9/24/2018

//This program displys the slang term for a dice roll using only if and if-else statements.

import java.util.Scanner;//Imports the scanner class for user input

public class CrapsIf{
  public static void main(String[] args){//Start of the main method
    
    Scanner input = new Scanner(System.in);//Creates a new scanner named input
    
    int dice1 = 0;
    int dice2 = 0;
    
    System.out.print("Enter '1' to randomly roll the dice and enter '2' to input the dice yourself ");
    int choice = input.nextInt();
    if (choice==1){//Randomly rolls dice if the user puts in 1
      dice1 = (int)(Math.random()*6)+1;
      dice2 = (int)(Math.random()*6)+1;
    } 
    else{//Allows user to input dice if he/she doesn't enter 1
      System.out.print("Enter the value of the first die ");
      dice1 = input.nextInt();
      System.out.print("Enter the value of the second die ");
      dice2 = input.nextInt();
    }
    
    
    if(dice1>6||dice1<1||dice2>6||dice2<1){//Displays invalid dice if the user inputs integers not between 1-6
      System.out.println("Invalid Dice");
    }
    else{//This else statement only works if the number on each die is between 1-6
    
      System.out.println("First dice = " + dice1 +" Second Dice =  " + dice2);//Prints out the roll in terms of numbers
     //The next bunch of if statements gives the slang name for the roll
      if(dice1==1 && dice2==1){
        System.out.println("Snake Eyes");
      }
      if((dice1==1 && dice2==2)||(dice1==2 && dice2==1)){
        System.out.println("Ace Deuce");
      }
      if((dice1==1 && dice2==3)||(dice1==3 && dice2==1)){
        System.out.println("Easy Four");
      }
      if((dice1==1 && dice2==4)||(dice1==4 && dice2==1)){
        System.out.println("Fever Five");
      }
      if((dice1==1 && dice2==5)||(dice1==5 && dice2==1)){
        System.out.println("Easy Six");
      }
      if((dice1==1 && dice2==6)||(dice1==6 && dice2==1)){
        System.out.println("Seven Out");
      }
      if(dice1==2 && dice2==2){
        System.out.println("Hard Four");
      }
      if((dice1==2 && dice2==3)||(dice1==3 && dice2==2)){
        System.out.println("Fever Five");
      }
      if((dice1==2 && dice2==4)||(dice1==4 && dice2==2)){
        System.out.println("Easy Six");
      }
      if((dice1==2 && dice2==5)||(dice1==5 && dice2==2)){
        System.out.println("Seven Out");
      }
      if((dice1==2 && dice2==6)||(dice1==6 && dice2==2)){
        System.out.println("Easy Eight");
      }
      if(dice1==3 && dice2==3){
        System.out.println("Hard Six");
      }
      if((dice1==3 && dice2==4)||(dice1==4 && dice2==3)){
        System.out.println("Seven Out");
      }
      if((dice1==3 && dice2==5)||(dice1==5 && dice2==3)){
        System.out.println("Easy Eight");
      }
      if((dice1==3 && dice2==6)||(dice1==6 && dice2==3)){
        System.out.println("Nine");
      }
      if(dice1==4 && dice2==4){
        System.out.println("Hard Eight");
      }
      if((dice1==4 && dice2==5)||(dice1==5 && dice2==4)){
        System.out.println("Nine");
      }
      if((dice1==4 && dice2==6)||(dice1==6 && dice2==4)){
        System.out.println("Easy Ten");
      }
      if(dice1==5 && dice2==5){
        System.out.println("Hard Ten");
      }
      if((dice1==5 && dice2==6)||(dice1==6 && dice2==5)){
        System.out.println("Yo-leven");
      }
      if(dice1==6 && dice2==6){
        System.out.println("Boxcars");
      }
    }//end of the else statement
    
  
  }//end of the main method
}//end of the class