//John Taulane
//CSE002
//9/24/2018

//This program displys the slang term for a dice roll using only if and if-else statements.

import java.util.Scanner;//Imports the scanner class for user input

public class CrapsSwitch{
  public static void main(String[] args){//Start of the main method
    
    Scanner input = new Scanner(System.in);//Creates a new scanner named input
    
    int dice1 = 0;//Initializes and declares the first dice roll
    int dice2 = 0;//Initializes and declares the secon dice roll
    
    System.out.print("Enter '1' to randomly roll the dice and enter '2' to input the dice yourself ");
    int choice = input.nextInt();//int choice is what decides whether or not to randomly roll the dice
    
    switch(choice){//This switch has two branches one for if case is 1 and one for if case is 2
      case 1:
        dice1 = (int)(Math.random()*6)+1;
        dice2 = (int)(Math.random()*6)+1;
        break;
      case 2:
        System.out.print("Enter the value of the first die ");
        dice1 = input.nextInt();
        System.out.print("Enter the value of the second die ");
        dice2 = input.nextInt();
        break;
      default://If the user doesn't enter 1 or 2 it will display "invalid entry"
        System.out.println("Invalid entry");
    }
    
    switch(dice1){//This switch statement says what to do depending on what dice 1 is
      case 1:
        switch(dice2){//This switch statement tells what to do depending on dice2 if dice1 is 1
          case 1:System.out.println("Snake Eyes");
            break;
          case 2:System.out.println("Ace Deuce");
            break;
          case 3:System.out.println("Easy Four");
            break;
          case 4:System.out.println("Fever Five");
            break;
          case 5:System.out.println("Easy Six");
            break;
          case 6:System.out.println("Seven Out");
            break;
          default:System.out.println("Invalid Second Die");
            break;
        }
        break;
      case 2:
        switch(dice2){//if dice1 = 2...
          case 1:System.out.println("Ace Deuce");
            break;
          case 2:System.out.println("Hard Four");
            break;
          case 3:System.out.println("Fever Five");
            break;
          case 4:System.out.println("Easy Six");
            break;
          case 5:System.out.println("Seven Out");
            break;
          case 6:System.out.println("Easy Eight");
            break;
          default:System.out.println("Invalid Second Die");
            break;
        }
        break;
      case 3:
        switch(dice2){//If dice1 = 3...
          case 1:System.out.println("Easy Four");
            break;
          case 2:System.out.println("Fever Five");
            break;
          case 3:System.out.println("Hard Six");
            break;
          case 4:System.out.println("Seven Out");
            break;
          case 5:System.out.println("Easy Eight");
            break;
          case 6:System.out.println("Nine");
            break;
          default:System.out.println("Invalid Second Die");
            break;
        }
        break;
      case 4://If dice1 = 4...
        switch(dice2){
          case 1:System.out.println("Fever Five");
            break;
          case 2:System.out.println("Easy Six");
            break;
          case 3:System.out.println("Seven Out");
            break;
          case 4:System.out.println("Hard Eight");
            break;
          case 5:System.out.println("Nine");
            break;
          case 6:System.out.println("Easy Ten");
            break;
          default:System.out.println("Invalid Second Die");
            break;
        }
        break;
      case 5:
        switch(dice2){
          case 1:System.out.println("Easy Six");
            break;
          case 2:System.out.println("Seven Out");
            break;
          case 3:System.out.println("Easy Eight");
            break;
          case 4:System.out.println("Nine");
            break;
          case 5:System.out.println("Hard Ten");
            break;
          case 6:System.out.println("Yo-Leven");
            break;
          default:System.out.println("Invalid Second Die");
            break;
        }
        break;
      case 6:
        switch(dice2){
          case 1:System.out.println("Seven Out");
            break;
          case 2:System.out.println("Easy Eight");
            break;
          case 3:System.out.println("Nine");
            break;
          case 4:System.out.println("Easy Ten");
            break;
          case 5:System.out.println("Yo-Leven");
            break;
          case 6:System.out.println("Boxcars");
            break;
          default:System.out.println("Invalid Second Die");
            break;
        }
        break;
      default:
        System.out.println("Invalid First Dice");//If dice1 doesn't equal 1-6
        break;
    }
    
  }
}
  