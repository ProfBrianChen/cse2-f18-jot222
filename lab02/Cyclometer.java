//This program prints the number of minutes, the number of counts,
//the number of miles, and the combined distance for 2 seperate bicycle trips

//John Taulane
//CSE002
//Date: 9/7/2018

public class Cyclometer{
  //Below begins the main method, which is part of every java program
  public static void main (String[] args){
    int secTrip1 = 408;
    int secTrip2 = 3220;  //These two integer variables are declared and assigned values for how many seconds each of the trips took
    int countsTrip1 = 1561;
    int countsTrip2 = 9037;  //These two integer varables are declared and assigned values for how many times the front wheel rotated
    
    double wheelDiameter = 27.0;  
    double PI = 3.14159;
    int feetPerMile = 5280;
    int inchesPerFoot = 12;
    int secondsPerMinute = 60;    // lines 16-20 declare and assign values for the wheel diameter, other calculations to be used later on
    double distanceTrip1;         // declares double variable to be used later on
    double distanceTrip2;         // declares double variable to be used later on
    double totalDistance;         // declares double variable to be used later on
    
    System.out.println("Trip 1 took " + (secTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts");
    System.out.println("Trip 2 took " + (secTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts");
    //Prints out the time in minutes and amount of rotations for each indivual trips
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    //gives distance for the first trip in inches
    distanceTrip1 /= inchesPerFoot * feetPerMile;
    //converts inches to miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    
    totalDistance = distanceTrip2 + distanceTrip1;
    //Combines the two trips as the double variable totalDistance
    
  System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	System.out.println("The total distance was " + totalDistance + " miles");

  
  }//This bracket ends the main method
  
}//This bracket ends the class
