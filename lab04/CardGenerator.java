//John Taulane
//CSE002
//9/21/2018

//This program randomly generates a card from a deck of 52 cards and displays what card it is

public class CardGenerator{
  public static void main(String[] args){//Start of the main method
    
    int rand = (int)(Math.random()*51)+1;//Randomly assigns an integer between 1 and 52 to variable rand
    
    
    String suit=" ";    
    String cardIdentity = " ";//Initialize the suit and cardIdentity strings
    
    if (rand<=13){
      suit = "Diamonds";
      }
  
    if (rand>=14 && rand<=26){
      suit = "Clubs";
    }
  
    if (rand>=27 && rand<=39){
      suit = "Hearts";
    }
  
    if(rand>=40){
     suit = "Spades";
    }
    //Lines 16-31 gives the card the suit by making the first 13 diamonds, the next 13 clubs, etc.
    int actualCard = rand % 13;
    //Uses the modulus operator to help assign the face values of the cards
   
    switch(actualCard){//Start of the switch statement. Used to give the cardIdentity string a value based on what random integer
                       // was picked
      case 1: cardIdentity = "ace";
        break;
      case 2: cardIdentity = "2";
        break;
      case 3: cardIdentity = "3";
        break;
      case 4: cardIdentity = "4";
        break;
      case 5: cardIdentity = "5";
        break;
      case 6: cardIdentity = "6";
        break;
      case 7: cardIdentity = "7";
        break;
      case 8: cardIdentity = "8";
        break;
      case 9: cardIdentity = "9";
        break;
      case 10: cardIdentity = "10";
        break;
      case 11: cardIdentity = "jack";
        break;
      case 12: cardIdentity = "queen";
        break;
      case 13: cardIdentity = "king";
        break;
    }//End of the switch statement
    System.out.println("Your card is the " + cardIdentity +" of " + suit);//Prints the card and it's suit
  }//End of the main method
}//End of the class