import java.util.Random;
public class lab08{
  public static void main(String[] args){
    
    Random randGen = new Random();
    
    int[] myArray = new int[100];
    int[] ocurrenceArray = new int[100];
    
    for(int i=0; i<myArray.length;i++){
      myArray[i] = randGen.nextInt(100);
    }
    
    for(int i=0;i<100;i++){
      for(int k=0;k<100;k++){
        if(myArray[k]==i){
          ocurrenceArray[i]++;
        }
      }
    }
    
    for(int i = 0; i<100; i++){
      if(ocurrenceArray[i]!=0){
        System.out.println(i + " ocurrs "+ ocurrenceArray[i] + " times."); 
      }
    }
      
    
  }
}