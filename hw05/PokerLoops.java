//John Taulane
//   10/9/2018
//CSE002

//This program randomly creates a hand of 5 playing cards and does this a number of times based on user input. Then it calculates the odds of ocurring for particular hands

import java.util.Scanner;

public class PokerLoops{
  public static void main(String[] args){ 
    int card1=0;
    int card2=0;
    int card3=0;
    int card4=0;
    int card5=0;
    int numHands=0;
    int check1 = 0;
    int onePairCounter=0;
    int twoPairCounter=0;
    int threeOfAKindCounter=0;
    int fourOfAKindCounter=0;
  
    Scanner myScanner = new Scanner(System.in);
    
    while(check1==0){//This while statement creates a loop if the user does not input an intger and repeats the question until an integer is input
      System.out.println("How many hands of 5 cards do you wish to generate?");
      boolean isInteger = myScanner.hasNextInt();   //isInteger is true when the user inputs an integer
      if(isInteger){
        numHands=myScanner.nextInt();
        check1=2;//Exits the while loop
      }
      else{    //Makes sure the user stays in the while loop when there isn't an inter input.
        myScanner.next();
        System.out.println("Please enter an integer");
      }
     
      for(int i=1;i<=numHands;i++){   //This loop runs the amount of times that the user designates in the scanner above
        card1 =(int)((Math.random()*51)+1);//Randomly assigns an integer from 1-52
        card2 =(int)((Math.random()*51)+1);
        card3 =(int)((Math.random()*51)+1);
        card4 =(int)((Math.random()*51)+1);
        card5 =(int)((Math.random()*51)+1);
//The next if statement and else if statements insure that two of the same card isn't in the hand(Ex: two ace of spades are dealt)
        if((card1==card2)||(card1==card3)||(card1==card4)||(card1==card5)){
          i--;//The i-- makes sure that hands with two of the same card don't count as a generated hand
        }
        else if((card2==card3)||(card2==card4)||(card2==card5)){
          i--;
        }
        else if ((card3==card4)||(card3==card5)){
          i--;
        }
        else if(card4==card5){
          i--;
        }
        else{//Go into the else statement when none of the cards are the exact same card.
          card1 = card1 % 13 +1;   //Modulus 13 makes it so the program can read the 4 of diamonds and the 4 of jacks read as a pair
          card2 = card2 % 13 +1;
          card3 = card3 % 13 +1;
          card4 = card4 % 13 +1;
          card5 = card5 % 13 +1;
          
        /*  System.out.println(card1);
          System.out.println(card2);
          System.out.println(card3);
          System.out.println(card4);
          System.out.println(card5);*/
        }
        
        for(int j=1;j<=13;j++){//This for loop runs through all of the possible cards and checks whether the hand has a four of akind, three of a kind,etc...
          if((j==card1 && j==card2 && j==card3 && j==card4)||(j==card1 && j==card2 && j==card3 && j==card5)||(j==card1 && j==card2 && j==card4 && j==card5)
               ||(j==card1 && j==card3 && j==card4 && j==card5)||(j==card2 && j==card3 && j==card4 && j==card5)){
            fourOfAKindCounter++;
          }
          else if((j==card1 && j==card2 && j==card3)||(j==card1 && j==card2 && j==card4)||(j==card1 && j==card2 && j==card5)||(j==card1 && j==card3 && j==card4)||
                  (j==card1 && j==card3 && j==card5)||(j==card2 && j==card3 && j==card4)||(j==card2 && j== card3 && j== card5)||(j==card3 && j==card4 && j==card5)){
            threeOfAKindCounter++;
          }
          else if((j==card1 && j==card2)||(j==card1 && j==card3)||(j==card1 && j==card4)||(j==card1 && j==card5)||(j==card2 && j==card3)||(j==card2 && j==card4)||
                  (j==card2 && j==card5)||(j==card3 && j==card4)||(j==card3 && j==card5)||(j==card4 && j==card5)){
            onePairCounter++;
          }
        
        }
                                                                                                          
                                                                                                                       
      }
    System.out.println("Number of hands generated: "+numHands);
    System.out.println("Number of 4 of a kinds: "+fourOfAKindCounter);
    System.out.println("Number of 3 of a kinds: "+threeOfAKindCounter);
    System.out.println("Number of one pairs: "+onePairCounter);
    System.out.println("");
    
    double oddsFourOfAKind = (double)fourOfAKindCounter / (double)numHands;//Calculates the odds of a four-of-a-kind and makes it a double
    double oddsThreeOfAKind = (double)threeOfAKindCounter / (double)numHands;
    double oddsOnePair = (double)onePairCounter / (double)numHands;
    
    int trimFourOfAKind = (int)(oddsFourOfAKind*1000.0);//The next two lines make it so that the odds of a four of a kind is cut off after three decimal places
    oddsFourOfAKind = ((double)trimFourOfAKind)/1000.0;
    
    int trimThreeOfAKind = (int)(oddsThreeOfAKind*1000.0);//These two lines cut off the odds of three-of-a-kind after 3 decimal digits
    oddsThreeOfAKind = ((double)trimThreeOfAKind)/1000.0;
    
    int trimOnePair = (int)(oddsOnePair*1000.0);//Trims the odds of one pair after three decimal places
    oddsOnePair = ((double)trimOnePair)/1000.0;
    
    System.out.println("Odds of a 4-of-a-kind: "+oddsFourOfAKind);
    System.out.println("Odds of a 3-of-a-kind: "+oddsThreeOfAKind);
    System.out.println("Odds of one-pair: "+oddsOnePair);
    }
  
  
  }
}