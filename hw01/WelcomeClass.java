////////John Taulane
/////// 9/3/2018
///////CSE002 Section 110

//////Program displays a welcome display using my Lehigh network ID

public class WelcomeClass{
  
  public static void main(String args[]){
    System.out.println("-----------");
    System.out.println("I WELCOME I");
    System.out.println("-----------");
////The 3 lines above display the generic welcome message////
    System.out.println("^  ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\/ \\");
////In the line above two forward slashes are used to output ONE forward slash in the output
    System.out.println("<--J--O--T--2--2--2-->");
////Prints the unique Lehigh Network ID part of the message/////
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /\\ /");
///Again forward slashes are doubled to output one/////
    System.out.println(" v  v  v  v  v  v  v");      
////Outputs the final line of the welcome class signature
    
    
  }
}

