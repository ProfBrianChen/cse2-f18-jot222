//John Taulane
//10-27-2018
//HW07
//This program allows the user to input text and then gives the user options in a menu to analyze the user-inputted text
import java.util.Scanner;

public class wordTools{
  
  public static void main(String [] args){
    String menuChoice="";
    String userText="";
    int numNonWS=0;//Number of non whitespace words
    int numWords=0;//Number of words
    
    userText = sampleText();//Calls the sample text method to store whatever the user inputs as a string
    
    do{//The do-while loop insures that the menu is printed and continues to print until the user quits it
      menuChoice = printMenu();//Prints the menu
      
      if(menuChoice.equals("c")){//when c is input calls the nonwhitespace method
        numNonWS = getNumOfNonWSCharacters(userText);
        System.out.println("Number of nonwhitespace characters: "+ numNonWS);
      }
      
      if(menuChoice.equals("w")){//when w is input calls the getNumWords method
        numWords = getNumWords(userText);
        System.out.println("Number of words: "+ numWords);
      }
      
      if(menuChoice.equals("f")){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("What word do you want to find?");
        String wordToFind = myScanner.nextLine();
        int numInstances = findText(wordToFind, userText);//Stores the return value of the findtext method as numInstances and passes the strings wordToFind and userText to it
        System.out.println("Occurrences of "+wordToFind + ": "+numInstances);//
      }
      if(menuChoice.equals("r")){
        String editedText = replaceExclamation(userText);
        System.out.println("Edited Text: "+editedText);
      }
      if(menuChoice.equals("s")){
        String editedText = shortenSpaces(userText);
        System.out.println("Edited text: "+editedText);
      }
      
    }while(!menuChoice.equals("q"));//Makes sure the menu prints until the user presses q to quit.
  
  }//End of the main method
  
  public static String sampleText(){//Method that gets the user inputted text and stores it as a string
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a sample text");
    String userText = myScanner.nextLine();
    System.out.println("You entered: " + userText);
    return userText;//Returns what the user put in as  a string
  }
  
  public static String printMenu(){//Prints the menu
    Scanner myScanner = new Scanner(System.in);
    String menuChoice = "c";
    System.out.println("********************************************");
    System.out.println("Menu");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - quit");
    System.out.println("");
    System.out.println("Pick an option");
    System.out.println("********************************************");
    
    menuChoice = myScanner.nextLine();//Stores the users choice of menu option
    return menuChoice;//Returns the users choice to the main method
  }
  
  public static int getNumOfNonWSCharacters(String userText){
    int length = userText.length();//Finds the length of the user's text
    int numCharsNotWhitespace=length;
    
    for(int i=0; i < length; i++){
      if(userText.charAt(i)==(' ')){//Goes through each index and everytime there is a whitespace it subtracts from the number of characters that aren't whitespace
        -- numCharsNotWhitespace;
      }
    }
    return numCharsNotWhitespace;
  }
  
  public static int getNumWords(String userText){
    int numWords =1;//Starts as 1 because the last word won't have a space at the end
    int length = userText.length();
    
    for(int i=0; i < length; i++){
      if(userText.charAt(i)==(' ') && userText.charAt(i+1)!=(' ')){
        ++ numWords;//Counter increases when there is a space after a word and the character after isn't a space
      }
    }
    return numWords;
  }
  
  public static int findText(String textToFind, String userText){
    int occurrences=0;
    int index=0;
    int lengthOfTextToFind = textToFind.length();
    
    while(index>=0){
      index = userText.indexOf(textToFind,index);//Stores the index of where the wordToFind is located
      
      if(index>= 0){
        ++ occurrences;//Increases occurrences when the wordToFind is present
        index += lengthOfTextToFind;
      }
    }
    return occurrences;
  }
  
  public static String replaceExclamation(String userText){
    userText = userText.replace('!','.');
    return userText;
  }
  
  public static String shortenSpaces(String userText){
    int length = userText.length();
    for(int i =0; i<length;i++){//This loop repeats the amount of times that the length of the string from the user is
      userText = userText.replace("  "," ");//Replaces sets of two spaces with one space. This repeats over and over until there are only single spaces left
    }
    return userText;//Returns the new text with shortened spaces to the main method
  }


}